## 2.5.0

New supported features:
* Added support for RT Packages and Exe
* Added support for French, German, Japanese, Korean, and Chinese versions of LabVIEW (previously, some builds would fail in these environments due to differences in the build spec)

The following reported bugs have all been fixed:
* Unable to explore to destination when build spec destination is absolute (and other problems related to relative/absolute paths).
* The version number saved in the lvsln file is not populated in the UI when the lvsoln is loaded from solution explorer vs double clicking a lvsln file on disk
* Sub-Solutions always build as debug when using right-click shortcut to "Rebuild"